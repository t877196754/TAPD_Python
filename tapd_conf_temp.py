#!/usr/bin/python
# -*- coding: utf-8 -*-

# @date: 2022/1/13 9:23

# @Author : wuhuanbuai
# @Email : 877196754@qq.com
# @File : tapd_conf_temp.py

#todo 这是配置文件示范，请复制一份命名为 “tapd_conf.py”即可

TAPD_USER='tapd@tapd.com'
TAPD_PASSWORD='**********'

#是否需要判断是否的登录，去掉耗时
TAPD_IF_LOGIN=False
TAPD_COM_NAME='***科技股份有限公司' #公司名称
TAPD_COM_URL= 'https://www.tapd.cn/***/workspaces/workspace_edit' #TAPD登录进去首页
TAPD_ORGANIZATION_ID='****'  #用户查询时候的企业ID，20位
TAPD_ORGANIZATION_ID_SHORT='****' #企业ID,在主界面上可以看到，8位数
