# TAPD 人员信息管理脚本

#### 介绍
tapd官方的api只能管理项目，所以就做了接口，后续对接前端图形化界面，进行人员管理，包括昵称、姓名、邮箱、人员角色等用户信息修改。
注意：暂时不会考虑做项目的管理，毕竟tapd开放的API已经有了，基于官方的API肯定更好。

#### 软件架构
1. 用phantomjs进行模拟登录，然后后面采用requests模块进行操作。
这样兼容便捷性和代码高效性。
2. 考虑到频繁登录可能引起的问题，所以每次登录成功后，cookies都会本地保留，下次调用直接载入cookies,进行操作。
   所以在高并发和多线程情况下，也是可以使用的。


#### 安装教程

1.  安装依赖
2.  配置文件
3.  运行主程序

#### 使用说明

1.  配置tapd_conf_temp.py 改成自己的配置，然后拷贝一份 命名为tapd_conf.py
2.  下载后直接运行TAPD_M.py 即可
2.  目前实现人员搜索按昵称、真实姓名或者邮箱。
3.  根据搜索结果进行邮箱、姓名、昵称、角色设置。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
